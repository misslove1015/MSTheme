## MSTheme

### 介绍    
主题颜色基础库，包含基本颜色

### 使用方法  
1.Podfile 添加下面代码，执行 pod install   

	 ms_pod "MSTheme", :tag => "v1.0", :git => "git@gitlab.com:misslove1015/MSTheme.git", :inhibit_warnings => false , :ms_debug => $msDebugSwitch, :ms_branch => $msBranch, :ms_useRemote => $msUseRemote
    
2.引入头文件
 
    #import <MSTheme/MSTheme.h>
   

