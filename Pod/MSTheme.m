//
//  MSTheme.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "MSTheme.h"
#import "MSFoundation.h"
#import "MSThemeDefine.h"

@implementation MSTheme

+ (instancetype)theme {
    static dispatch_once_t onceToken;
    static MSTheme *theme = nil;
    dispatch_once(&onceToken, ^{
        theme = [[MSTheme alloc]init];
    });
    return theme;
}

- (UIColor *)defaultColor {
    if (!_defaultColor) {
        _defaultColor = COLOR_HEX(0xFFDA44);
    }
    return _defaultColor;
}

- (UIColor *)vcBgColor {
    if (!_vcBgColor) {
        _vcBgColor = COLOR_HEX(0xF1F2F3);
    }
    return _vcBgColor;
}

- (UIColor *)blackColor {
    if (!_blackColor) {
        _blackColor = COLOR_HEX(0x333333);
    }
    return _blackColor;
}

- (UIColor *)lightBlackColor {
    if (!_lightBlackColor) {
        _lightBlackColor = COLOR_HEX(0x666666);
    }
    return _lightBlackColor;
}

- (UIColor *)lightGrayColor {
    if (!_lightGrayColor) {
        return COLOR_HEX(0x999999);
    }
    return _lightGrayColor;
}

- (UIColor *)disableColor{
    if (!_disableColor) {
        _disableColor = COLOR_HEX(0xDADBDA);
    }
    return _disableColor;
}

- (UIColor *)separatorColor {
    if (!_separatorColor) {
        _separatorColor = COLOR_HEX(0xF3F3F3);
    }
    return _separatorColor;
}

- (UIColor *)navBgColor {
    if (!_navBgColor) {
        _navBgColor = COLOR_HEX(0xFDD843);
    }
    return _navBgColor;
}

- (UIColor *)navTitleColor {
    if (!_navTitleColor) {
        _navTitleColor = COLOR_HEX(0x333333);
    }
    return _navTitleColor;
}

- (UIImage *)navBackImage {
    if (!_navBackImage) {
        _navBackImage = MSThemeImageNamed(@"ms_nav_back");
    }
    return _navBackImage;
}


@end
