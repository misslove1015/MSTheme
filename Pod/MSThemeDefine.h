//
//  MSThemeDefine.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#ifndef MSThemeDefine_h
#define MSThemeDefine_h

// bundle 名字
#define MSTheme_BundleName        @"MSTheme.bundle"

// 图片宏定义
#define MSThemeImageNamed(name) [UIImage imageNamed:name]?[UIImage imageNamed:name]:[UIImage imageWithContentsOfFile:[[[NSBundle bundleForClass:[self class]] resourcePath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@/%@",MSTheme_BundleName,name]]]

#endif /* MSThemeDefine_h */
