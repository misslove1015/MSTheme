//
//  MSTheme.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/6.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MSTheme : NSObject

+ (instancetype)theme;

@property (nonatomic, strong) UIColor *defaultColor;    // 主色调
@property (nonatomic, strong) UIColor *vcBgColor;       // vc 背景色
@property (nonatomic, strong) UIColor *blackColor;      // 黑色
@property (nonatomic, strong) UIColor *lightBlackColor; // 浅黑色
@property (nonatomic, strong) UIColor *lightGrayColor;  // 浅灰色
@property (nonatomic, strong) UIColor *disableColor;    // 不可用颜色
@property (nonatomic, strong) UIColor *separatorColor;  // 分隔线颜色
@property (nonatomic, strong) UIColor *navBgColor;      // 导航条背景色
@property (nonatomic, strong) UIColor *navTitleColor;      // 导航条背景色

@property (nonatomic, strong) UIImage *navBackImage;    // 导航条返回图片

@end
